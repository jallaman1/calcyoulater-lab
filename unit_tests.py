import server

def test_addition():
    assert server.addition(2, 3) == 5

def test_subtraction():
    assert server.subtraction(10, 3) == 7
